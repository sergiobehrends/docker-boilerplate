# Docker Boilerplate

## Run App
```
# Run Dev environment
# Add --build if required
docker-compose -f docker-compose.yml -f docker-compose.dev.yml up

# Run Production environment
docker-compose up
# OR
docker-compose -f docker-compose.yml up
```

## NPM Packages
```
# Add initial npm packages

docker-compose -f docker-compose.yml -f docker-compose.dev.yml run frontend /bin/bash

# Starts frontend container and from there

npm install package --save

```

```
# Add package on running container

docker exec -it dockerwatch_frontend_1 /bin/bash

npm install package --save

```