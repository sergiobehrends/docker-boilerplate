const Hapi = require('hapi');
const mongojs = require('mongojs');

const server = new Hapi.Server();
server.connection({ port: process.env.PORT || 3000 });

// Connect with the database
server.app.mongo = mongojs(process.env.MONGO_HOST + '/api');
const messages = server.app.mongo.collection('messages');

server.route({
  method: 'GET',
  path:'/user/{user}',
  handler: function (request, reply) {
    const user = encodeURIComponent(request.params.user);

    return reply({ hell: user});
  }
});

server.route({
  method: 'GET',
  path:'/messages',
  handler: function (request, reply) {
    messages.find((err, docs) => {
      if (err) {
        return reply({error: 'Mongo DB'});
      }

      reply(docs);
    });
  }
});

server.start((err) => {
  if (err) {
    throw err;
  }

  console.log('Server running at:', server.info.uri);
});